/*
 * Copyright (c) 2011 Vinay Pulim <vinay@milewise.com>
 * MIT Licensed
 */

require("es6-promise").polyfill();
// require("fetch-everywhere");
require('cross-fetch/polyfill')
var url = require("./url");

var VERSION = "0.2.0";

exports.request = function(rurl, data, callback, exheaders, exoptions) {
  var method = data ? "POST" : "GET";
  var headers = {
    "User-Agent": "browser-soap/" + VERSION,
    Accept: "text/html,application/xhtml+xml,application/xml",
    "Content-Type": "application/x-www-form-urlencoded"
  };

  exheaders = exheaders || {};
  for (var attr in exheaders) {
    headers[attr] = exheaders[attr];
  }

  fetch(rurl, {
    method: method,
    body: data,
    headers: headers
  }).then(function(response) {
    var resp = { statusCode: response.status };
    response.text().then(text => {
      if (response.status >= 400) {
        callback("Eroor:: " + response.status, resp, text);
      }
      callback(null, resp, text);
    });
  });
};
