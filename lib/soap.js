/*
 * Copyright (c) 2011 Vinay Pulim <vinay@milewise.com>
 * MIT Licensed
 */

var Client = require("./client").Client,
  open_wsdl = require("./wsdl").open_wsdl,
  WSDL = require("./wsdl").WSDL;

var _wsdlCache = {};

function _requestWSDL(url, options, callback) {
  if (typeof options === "function") {
    callback = options;
    options = {};
  }

  var wsdl = _wsdlCache[url];
  if (wsdl) {
    callback(null, wsdl);
  } else {
    open_wsdl(url, options, function(err, wsdl) {
      if (err) return callback(err);
      else _wsdlCache[url] = wsdl;
      callback(null, wsdl);
    });
  }
}

function createClient(url, options, callback, endpoint) {
  if (typeof options === "function") {
    endpoint = callback;
    callback = options;
    options = {};
  }
  endpoint = options.endpoint || endpoint;
  _requestWSDL(url, options, function(err, wsdl) {
    callback(err, wsdl && new Client(wsdl, endpoint));
  });
}

function BasicAuthSecurity(username, password) {
  this._username = username;
  this._password = password;
}

BasicAuthSecurity.prototype.addHeaders = function(headers) {
  headers["Authorization"] = "Basic " +
    new Buffer(this._username + ":" + this._password || "").toString("base64");
};

BasicAuthSecurity.prototype.toXML = function() {
  return "";
};

exports.BasicAuthSecurity = BasicAuthSecurity;
exports.createClient = createClient;
exports.WSDL = WSDL;
