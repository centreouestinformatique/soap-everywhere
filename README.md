# Soap-Everywhere

This module lets you connect to web services using SOAP, in pure javascript, using node, browser, or react-native.

This is a fork of `browser-soap`, which is a fork of `tinysoap`, which is a fork of `node-soap`.

The biggest difference is that it is using the `fetch` API to make requests, polyfilled with fetch-everywhere.

## Install

Install with [npm](http://github.com/isaacs/npm):

```
  npm install soap-everywhere
```

## Module

### soap.createClient(url, callback) - create a new SOAP client from a WSDL url

``` javascript
  var soap = require('soap-everywhere');
  var url = 'http://example.com/wsdl?wsdl';
  var args = {name: 'value'};
  soap.createClient(url, function(err, client) {
      client.MyFunction(args, function(err, result) {
          console.log(result);
      });
  });
```

## Client

An instance of Client is passed to the soap.createClient callback.  It is used to execute methods on the soap service.

### Client.describe() - description of services, ports and methods as a JavaScript object

``` javascript
  client.describe() // returns
    {
      MyService: {
        MyPort: {
          MyFunction: {
            input: {
              name: 'string'
            }
          }
        }
      }
    }
```

### Client.setSecurity(security) - use the specified security protocol (see WSSecurity below)

``` javascript
  client.setSecurity(new WSSecurity('username', 'password'))
```

### Client.*method*(args, callback) - call *method* on the SOAP service.

``` javascript
  client.MyFunction({name: 'value'}, function(err, result) {
      // result is a javascript object
  })
```
### Client.*service*.*port*.*method*(args, callback) - call a *method* using a specific *service* and *port*

``` javascript
  client.MyService.MyPort.MyFunction({name: 'value'}, function(err, result) {
      // result is a javascript object
  })
```
